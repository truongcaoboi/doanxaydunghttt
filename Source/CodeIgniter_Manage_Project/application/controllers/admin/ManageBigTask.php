<?php
class ManageBigTask extends CI_Controller{

	function __construct(){
		parent::__construct();
		$this->load->model('BigTask');
		$this->load->model('Subject');
		$this->load->model('Student');
		$this->load->model('SmallTask');
	}

	public function getGeneralDataBigTask(){
		$on_project = $_GET['on_project'];
		$data = array();
		$result = $this->BigTask->getGeneralDataBigTask($on_project);

		$dataBigTask = "";

		foreach ($result as $key => $rows_result){
			$dataBigTask = $dataBigTask."<tr><td><input type='checkbox' name='big_task_id' value='".$rows_result['big_task_id']."' 
			</td>";
			$dataBigTask = $dataBigTask."<td>".$rows_result['big_task_id']."</td>";
			$dataBigTask = $dataBigTask."<td>".$_GET['on_project']."</td>";
			$dataBigTask = $dataBigTask."<td>".$rows_result['big_task_des']."</td>";
			$dataBigTask = $dataBigTask."<td>".$rows_result['big_task_deadline']."</td>";
			$dataBigTask = $dataBigTask."<td><a href='http://localhost/CodeIgniter_Manage_Project/public/upload/".$rows_result['file_url']."'>".$rows_result['file_url']."</a></td>";
			$dataBigTask = $dataBigTask."<td class='option'> 
						   <a href='http://localhost/CodeIgniter_Manage_Project/admin/ManageBigTask/loadViewUpdateBigTask?big_task_id=".$rows_result['big_task_id']."&on_project=".$on_project. "' title='Chỉnh sửa'>
						   <img src='http://localhost/CodeIgniter_Manage_Project/public/admin_dashboard/images/icons/color/edit.png'/></a>
						   <a href='http://localhost/CodeIgniter_Manage_Project/admin/ManageBigTask/deleteBigTask?big_task_id=".$rows_result['big_task_id']."&on_project=".$on_project."' title='Xóa'> <img src='http://localhost/CodeIgniter_Manage_Project/public/admin_dashboard/images/icons/color/delete.png'/></a>
						   <a href='http://localhost/CodeIgniter_Manage_Project/admin/ManageBigTask/getAllInformationBigTaskById?big_task_id=".$rows_result['big_task_id']."'> <img src='http://localhost/CodeIgniter_Manage_Project/public/admin_dashboard/images/icons/color/view.png'/></a></td></tr>";
		}

		
		$data = $this->BigTask->variableStatic($on_project);
		$data['generalDataBigTask'] = $dataBigTask;
		$this->load->view('admin/main/main',$data);
	}

	public function deleteBigTask(){
		$this->BigTask->deleteBigTask($_GET['big_task_id']);
		$this->getGeneralDataBigTask();
	}

	public function loadViewUpdateBigTask(){
		$on_project = $_GET['on_project'];
		$data = array();
		$data = $this->BigTask->variableStatic($on_project);
		$data['data'] = 'admin/manage_big_task/UpdateForm';
		$data['on_project'] = $on_project;	
		$this->load->view('admin/main/main',$data);
	}

	public function updateBigTask(){
		$bigTask = array();
		$bigTask['on_project'] = $_POST['on_project'];
		$bigTask['big_task_des'] = $_POST['big_task_des'];
		$bigTask['big_task_deadline'] = $_POST['big_task_deadline'];

		$this->BigTask->updateBigTask($_GET['big_task_id'],$bigTask);
		$this->getGeneralDataBigTask();
	}

	public function loadViewAddBigTask(){
		$on_project = $_GET['on_project'];
		$data = array();
		echo $on_project;
		$data = $this->BigTask->variableStatic($on_project);
		$data['data'] = 'admin/manage_big_task/AddForm';
		$data['on_project'] = $on_project;
		$this->load->view('admin/main/main',$data);
	}

	public function addBigTask(){
		$bigTask = array();
		$bigTask['on_project'] =  $_POST['on_project'];
		$bigTask['big_task_des'] = $_POST['big_task_des'];
		$bigTask['big_task_deadline'] = $_POST['big_task_deadline'];

		if (!empty($_FILES['file_url']['name'])) {
	        $config['upload_path'] = FCPATH.'/public/upload';
	        $config['allowed_types'] = 'docx|doc|pdf';
	        $config['file_name'] = $_FILES['file_url']['name'];

	        $this->load->library('upload', $config);
	        $this->upload->initialize($config);

	        if ($this->upload->do_upload('file_url')) {
		          $uploadData = $this->upload->data();
		          $bigTask["file_url"] = $uploadData['file_name'];
		    } else {
	          	$bigTask["file_url"]  = "deo up dc";
	        }
	    } else {
	        $bigTask["file_url"] = 'ddeos cos file';
	    }


		// if (!empty($_FILES['file_url'])) {
		// 	$name = $_FILES['file_url']['name'];
			
		// 	$valid = True;
		// 	$dir = FCPATH."/public/upload/";
		// 	$tmp = $_FILES['file_url']['tmp_name'];
		// 	$size = $_FILES['file_url']['size'];
		// 	$url = $dir.$name;
	
		// 	if (!file_exists($tmp)) {
		// 		$valid = False;
		// 	}
		// 	if ($valid) { 
		// 		if (move_uploaded_file($tmp, $url)) {
		// 			$bigTask["file_url"] = $url;
		// 		}
		// 		else {
		// 			$bigTask["file_url"]  = "deo up dc";
		// 		}
		// 	}

			

		// }
		// else {
		// 	$bigTask["file_url"]  = "deo co file dc";
		// }
		$this->BigTask->addBigTask($bigTask);
		$this->getGeneralDataBigTask();
	}

	public function getAllInformationBigTaskById(){
		$big_task_id =$_GET['big_task_id'];
		$result = $this->SmallTask->getGeneralDataSmallTask($big_task_id);
		$dataSmallTask = "";

		foreach ($result as $key => $rows_result){
			$dataSmallTask = $dataSmallTask."<tr><td><input type='checkbox' name='small_task_id' value='".$rows_result['big_task_id']."' 
			</td>";
			$dataSmallTask = $dataSmallTask."<td>".$rows_result['small_task_id']."</td>";
			$dataSmallTask = $dataSmallTask."<td>".$_GET['big_task_id']."</td>";
			$dataSmallTask = $dataSmallTask."<td>".$rows_result['small_task_des']."</td>";
			$dataSmallTask = $dataSmallTask."<td>".$rows_result['small_task_deadline']."</td>";
			$dataSmallTask = $dataSmallTask."<td>".$rows_result['status']."</td>";
			$student_name  = $this->Student->getStudentNameById($rows_result['student_id']);
			$dataSmallTask = $dataSmallTask."<td>".$student_name[0]['student_name']."</td>";
		}
		$data['menu'] = "Quản Lí Task Đồ Án";
		$data['title'] = 'Danh Sách Small Task';
		$data['generalDataSmallTask'] = $dataSmallTask;
		$data['status_menu'] = array("exp","exp","active exp","exp","exp");
		$data['link_add'] = '#';
		$data['data'] = 'admin/manage_big_task/LoadDataSmallTask';
		$this->load->view('admin/main/main',$data);
		
	}
}
?>