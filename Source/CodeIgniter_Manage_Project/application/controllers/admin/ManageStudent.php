<?php
class ManageStudent extends CI_Controller{
	function __construct(){
		parent::__construct();
		$this->load->model('Project');
		$this->load->model('Subject');
		$this->load->model('Student');
	}

	public function getGeneralDataStudent(){

		$data = array();
		$result = $this->Student->getAllDataStudent();

		$dataStudent = "";

		foreach ($result as $key => $rows_result){
			$dataStudent = $dataStudent."<tr><td><input type='checkbox' name='student_id' value='".$rows_result['human_id']."' 
			</td>";
			$dataStudent = $dataStudent."<td>".$rows_result['human_code']."</td>";
			$dataStudent = $dataStudent."<td>".$rows_result['human_name']."</td>";
			$dataStudent = $dataStudent."<td>".$rows_result['human_course']."</td>";
			$dataStudent = $dataStudent."<td>".$rows_result['human_class']."</td>";
			$dataStudent = $dataStudent."<td class='option'> 
						   <a href='http://localhost/CodeIgniter_Manage_Project/admin/ManageStudent/loadViewupdateStudent?student_id=".$rows_result['human_id']. " '>
						   <img src='http://localhost/CodeIgniter_Manage_Project/public/admin_dashboard/images/icons/color/edit.png' title='Chỉnh sửa'/></a>
						   <a href='http://localhost/CodeIgniter_Manage_Project/admin/ManageStudent/deleteStudent?student_id=".$rows_result['human_id']. "'> <img src='http://localhost/CodeIgniter_Manage_Project/public/admin_dashboard/images/icons/color/delete.png' title='Xóa'/></a>
						   <a href='http://localhost/CodeIgniter_Manage_Project/admin/ManageStudent/getAllInformationStudentsById?student_id=".$rows_result['human_id']. "'> <img src='http://localhost/CodeIgniter_Manage_Project/public/admin_dashboard/images/icons/color/view.png' title='Xem chi tiết'/></a></td></tr>";
		}

		
		$data['menu'] = "Quản Lí Sinh Viên";
		$data['title'] = 'Danh Sách Sinh Viên';
		$data['status_menu'] =  array("exp","exp","exp","active exp","exp");
		$data['data'] = 'admin/manage_student/LoadData';
		$data['link_add'] = 'http://localhost/CodeIgniter_Manage_Project/admin/ManageStudent/loadViewAddStudent';
		$data['generalDataStudent'] = $dataStudent;
		$this->load->view('admin/main/main',$data);
	}

	public function deleteStudent(){
		$this->Student->deleteStudent($_GET['student_id']);
		$this->getGeneralDataStudent();
	}

	public function loadViewUpdateStudent(){
		$data = array();
		$data['subjects'] = $this->Subject->getSubjectInformation();
		$data['menu'] = "Quản Lí Sinh Viên";
		$data['title'] = 'Danh Sách Sinh Viên';
		$data['status_menu'] =  array("exp","exp","exp","active exp","exp");
		$data['link_add'] = 'http://localhost/CodeIgniter_Manage_Project/admin/ManageStudent/loadViewAddStudent';
		$data['data'] = 'admin/manage_student/UpdateForm';

		//load du lieu
		$student = $this->Student->getStudentById($_GET['student_id']);
		$data['student'] = $student;
		$this->load->view('admin/main/main',$data);

	}

	public function updateStudent(){
		$student = array();
		$student['human_code'] = $_POST['student_code'];
		$student['human_name'] = $_POST['student_name'];
		$student['human_course'] = $_POST['student_course'];
		$student['human_class'] = $_POST['student_class'];
		$student['human_email'] = $_POST['email'];
		$student['human_facebook'] = $_POST['facebook'];
		$student['human_phone'] = $_POST['phone'];
		//$student['user_type'] = $_POST['user_type'];

		$this->Student->updateStudent($_GET['student_id'],$student);
		$this->getGeneralDataStudent();
	}

	public function loadViewAddStudent(){
		$data = array();
		$data['subjects'] = $this->Subject->getSubjectInformation();
		$data['menu'] = "Quản Lí Sinh Viên";
		$data['status_menu'] =  array("exp","exp","exp","active exp","exp");
		$data['title'] = 'Danh Sách Sinh Viên';
		$data['data'] = 'admin/manage_student/LoadData';
		$data['link_add'] = 'http://localhost/CodeIgniter_Manage_Project/admin/ManageStudent/loadViewAddStudent';
		$data['data'] = 'admin/manage_student/AddForm';
		$this->load->view('admin/main/main',$data);
	}

	public function addStudent(){
		$student = array();
		$student['human_code'] = $_POST['student_code'];
		$student['human_name'] = $_POST['student_name'];
		$student['human_course'] = $_POST['student_course'];
		$student['human_class'] = $_POST['student_class'];
		$student['human_email'] = $_POST['email'];
		$student['human_facebook'] = $_POST['facebook'];
		$student['human_phone'] = $_POST['phone'];
		$student['human_type'] = 2;

		$user = array();
		$user['user_name'] = $_POST['student_code'];
		$user['pass_word'] = $_POST['student_code'];
		$user['user_type'] = $_POST['user_type']; 
		
		$this->Student->addStudent($student);
		$this->Student->createUser($user);
		$this->getGeneralDataStudent();
	}

	public function getAllInformationStudentsById(){
		$student_id = $_GET['student_id'];
		$data['menu'] = "Quản Lí Sinh Viên";
		$data['title'] = 'Thông Tin Sinh Viên';
		$data['status_menu'] =  array("exp","exp","exp","active exp","exp");
		$data['link_add'] = 'http://localhost/CodeIgniter_Manage_Project/admin/ManageStudent/loadViewAddStudent';
		$data['allDataStudent'] = $this->Student->getAllDataStudent();
		$data['subject_study'] = $this->Student->getSubjectStudentStudy($student_id);
		$data['data'] = 'admin/manage_student/DataEachStudent';

		//load du lieu
		$student = $this->Student->getStudentById($_GET['student_id']);
		$data['student'] = $student;
		$this->load->view('admin/main/main',$data);
	}

	public function Test(){
		$student_id = 20154219;
		$hc1 = $this->Student->getSubjectStudentStudy($student_id);
		foreach ($hc1 as $key => $project) {
			echo $project['subject_id'];
			echo "<br>";
		}
	}
} 
?>