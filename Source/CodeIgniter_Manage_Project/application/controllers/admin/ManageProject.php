<?php
class ManageProject extends CI_Controller{

	function __construct(){
		parent::__construct();
		$this->load->model('Project');
		$this->load->model('Subject');
		$this->load->model('Student');
	}

	public function getGeneralDataProject(){

		$data = array();
		$result = $this->Project->getGeneralDataProject();

		$dataProject = "";

		foreach ($result as $key => $rows_result){
			$dataProject = $dataProject."<tr><td><input type='checkbox' name='project_id' value='".$rows_result['project_id']."' 
			</td>";
			$dataProject = $dataProject."<td>".$rows_result['project_code']."</td>";
			$dataProject = $dataProject."<td>".$rows_result['project_name']."</td>";
			$dataProject = $dataProject."<td>".$rows_result['subject_id']."</td>";
			$dataProject = $dataProject."<td>".$rows_result['topic_name']."</td>";
			$dataProject = $dataProject."<td>".$rows_result['topic_des']."</td>";
			$dataProject = $dataProject."<td class='option'> 
						   <a href='http://localhost/CodeIgniter_Manage_Project/admin/ManageProject/loadViewUpdateProject?project_id=".$rows_result['project_id']. "'>
						   <img src='http://localhost/CodeIgniter_Manage_Project/public/admin_dashboard/images/icons/color/edit.png' title='Chỉnh sửa'/></a>
						   <a href='http://localhost/CodeIgniter_Manage_Project/admin/ManageProject/deleteProject?project_id=".$rows_result['project_id']."' > <img src='http://localhost/CodeIgniter_Manage_Project/public/admin_dashboard/images/icons/color/delete.png' title='Xóa'/></a>
						   <a href='http://localhost/CodeIgniter_Manage_Project/admin/ManageProject/getAllInformationProjectById?project_id=".$rows_result['project_id']."'> <img src='http://localhost/CodeIgniter_Manage_Project/public/admin_dashboard/images/icons/color/view.png' title='Xem chi tiết'/></a></td></tr>";
		}

		
		$data = $this->Project->variableStatic();
		$data['generalDataProject'] = $dataProject;
		$this->load->view('admin/main/main',$data);
	}

	public function deleteProject(){
		$this->Project->deleteProject($_GET['project_id']);
		$this->Student->deleteStudentOnProject($_GET['project_id']);
		$this->getGeneralDataProject();
	}

	public function loadViewUpdateProject(){
		$this->Student->deleteStudentOnProject($_GET['project_id']);
		$data = array();
		$data = $this->Project->variableStatic();
		$data['subjects'] = $this->Subject->getSubjectInformation();
		$data['data'] = 'admin/manage_project/UpdateForm';

		//load du lieu
		$project = $this->Project->getProjectById($_GET['project_id']);
		$data['project'] = $project;
		$this->load->view('admin/main/main',$data);

	}

	public function updateProject(){
		$project = array();
		$project['project_code'] = $_POST['project_code'];
		$project['project_name'] = $_POST['project_name'];
		$project['subject_id'] = $_POST['subject_id'];
		$project['topic_name'] = $_POST['topic_name'];
		$project['topic_des'] = $_POST['topic_des'];

		$this->Project->updateProject($_GET['project_id'],$project);

		// $student_project = array();
		// $student_project['student_id'] = $_GET['student_id'];
		// $student_project['project_id'] = (int)$_GET['project_id'];
		// $this->Student->addStudentToProject($student_project);

		$this->getGeneralDataProject();
	}

	public function loadViewAddProject(){
		$data = array();
		$data = $this->Project->variableStatic();
		$data['subjects'] = $this->Subject->getSubjectInformation();
		$data['data'] = 'admin/manage_project/AddForm';
		$data['project_id'] = $this->Project->getMaxIDProject();
		$this->load->view('admin/main/main',$data);
	}

	public function addProject(){
		$project = array();
		$project['project_code'] = $_POST['project_code'];
 		$project['project_name'] = $_POST['project_name'];
		//add subject_name by id
		$subject_id = $_POST['subject_id'];
		$subject_name = $this->Subject->getSubjectNameById($subject_id);
		$project['subject_id'] = $subject_name[0]['subject_name'];
		$project['topic_name'] = $_POST['topic_name'];
		$project['topic_des'] = $_POST['topic_des'];

		$this->Project->addProject($project);
		$this->getGeneralDataProject();
	}

	public function getProjectBySubject($subject_id){	
		$projects = array();
		$projects = $this->Project->getProjectBySubject($subject_id);
		echo "<select name='on_project' _autocheck='true' id='on_project' class='left'>";
		foreach ($projects as $key => $project) { 
			echo "<option value=".$project['project_id'].">".$project['project_name']."</option>";			
		}
		echo "</select>";
	}

	public function getHintStudent(){
		$hint = $_GET['hint'];
		$responseJSON = $this->Student->getHintStudent($hint);
		echo json_encode($arrayName = array('minhtu' => $responseJSON));
	}

	public function addStudentToProject(){
		$student_project = array();
		$student_project['student_id'] = $_GET['student_id'];
		$student_project['project_id'] = (int)$_GET['project_id'];
		$this->Student->addStudentToProject($student_project);
	}

	public function checkIdProject(){
		$project_id = $_GET['project_id'];
		$message = $this->Project->checkIdProject($project_id);
		echo $message;
		echo "<br>";
	}

	public function getAllInformationProjectById(){
		$project_id = $_GET['project_id'];
		$data = $this->Project->variableStatic();
		$data['generalDataProject'] = $this->Project->getAllInformationProjectById($project_id);//thong tin cua Project
		$data['information_student'] = $this->Student->getStudentOnProject($project_id);//danh sach sinh vein thuoc Project
		$data['data'] = 'admin/manage_project/DataEachProject';
		$this->load->view('admin/main/main',$data);
	}

	public function loadViewAddScoreToStudent(){
		$project_id = $_GET['on_project'];
		$students = $this->Student->getStudentOnProject($project_id);
		$dataString = "";
		foreach ($students as $key => $student){
			$dataString .= "<div class='formRow'>";
			$dataString .= "<label class='formLeft' for='student_".$student['student_id']."'>".$student['student_name']."<span class='req'>*</span></label>";
			$dataString .= "<div class='formRight'>";
			$dataString .= "<span class='oneTwo'><input name='student_".$student['student_id']."' _autocheck='true' type='text'/>";
			$dataString .= "<span name='name_autocheck' class='autocheck'></span>";
			$dataString .= "<div name='name_error' class='clear error'></div>";
			$dataString .= "</div>";
			$dataString .= "<div class='clear'></div>";
			$dataString .= "</div>";
		}

		$data = $this->Project->variableStatic();
		$data['form_add_score'] = $dataString;
		$data['data'] = 'admin/manage_project/AddScoreToStudent';
		$this->load->view('admin/main/main',$data);
	}

	public function addScoreToStudent(){
		$project_id = $_GET['on_project'];
		$students = $this->Student->getStudentOnProject($project_id);
		foreach ($students as $key => $student) {
			$str = "student_".$student['student_id'];
			$score = $_POST[$str];
			$this->Student->addScoreToStudent($project_id,$student['student_id'],$score);
		}
		$this->loadScoreStudent();
	}

	public function loadScoreStudent(){
		$project_id = $_GET['on_project'];
		$scores = $this->Student->getScoreStudent($project_id);
		$dataString = "";
		foreach ($scores as $key => $score) {
			$dataString .= "<div class='formRow'>";
			$dataString .= "<label class='formLeft' for='student_".$score['student_id']."'>".$score['student_name']."<span class='req'>*</span></label>";
			$dataString .= "<div class='formRight'>";
			$dataString .= "<span class='oneTwo'>".$score['score'];
			$dataString .= "<span name='name_autocheck' class='autocheck'></span>";
			$dataString .= "<div name='name_error' class='clear error'></div>";
			$dataString .= "</div>";
			$dataString .= "<div class='clear'></div>";
			$dataString .= "</div>";
		}

		$data = $this->Project->variableStatic();
		$data['score_student'] = $dataString;
		$data['data'] = 'admin/manage_project/ScoreStudent';
		$this->load->view('admin/main/main',$data);
	}
} 
?>