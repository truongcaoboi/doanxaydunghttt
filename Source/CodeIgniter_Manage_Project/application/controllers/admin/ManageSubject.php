<?php
class ManageSubject extends CI_Controller{

	function __construct(){
		parent::__construct();
		$this->load->model('Subject');
	}

	public function getAllDataSubject(){

		$data = array();
		$result = $this->Subject->getAllDataSubject();

		$dataSubject = "";

		foreach ($result as $key => $rows_result){
			$dataSubject = $dataSubject."<tr><td><input type='checkbox' name='subject_id' value='".$rows_result['subject_id']."' 
			</td>";
			$dataSubject = $dataSubject."<td>".$rows_result['subject_code']."</td>";
			$dataSubject = $dataSubject."<td>".$rows_result['subject_name']."</td>";
			$dataSubject = $dataSubject."<td>".$rows_result['subject_des']."</td>";
			$dataSubject = $dataSubject."<td class='option'> 
						   					<a 
						   						href='http://localhost/CodeIgniter_Manage_Project/admin/ManageSubject/loadViewUpdateSubject?subject_id=".$rows_result['subject_id']."'>
						   <img src='http://localhost/CodeIgniter_Manage_Project/public/admin_dashboard/images/icons/color/edit.png' title='Chỉnh sửa'/></a>
						   <a href='http://localhost/CodeIgniter_Manage_Project/admin/ManageSubject/deleteSubject?subject_id=".$rows_result['subject_id']."'> <img src='http://localhost/CodeIgniter_Manage_Project/public/admin_dashboard/images/icons/color/delete.png'
						   title='Xóa'/></a></td></tr>";
		}
		
		$data['menu'] = 'Quản Lí Môn Học';
		$data['title'] = 'Danh Sách Môn Học';
		$data['link_add'] = 'http://localhost/CodeIgniter_Manage_Project/admin/ManageSubject/loadViewAddSubject';
		$data['status_menu'] =  array("exp","active exp","exp","exp","exp");
		$data['allDataSubject'] = $dataSubject;
		$data['data'] = 'admin/manage_subject/LoadData';
		$this->load->view('admin/main/main',$data);
	}

	public function deleteSubject(){
		$this->Subject->deleteSubject($_GET['subject_id']);
		$this->getAllDataSubject();
	}

	public function loadViewUpdateSubject(){
		$data['menu'] = 'Quản Lí Môn Học';
		$data['title'] = 'Danh Sách Môn Học';
		$data['link_add'] = 'http://localhost/CodeIgniter_Manage_Project/admin/ManageSubject/loadViewAddSubject';
		$data['status_menu'] =  array("exp","active exp","exp","exp","exp");
		$data['data'] = 'admin/manage_subject/UpdateForm';

		//load dữ liệu
		$id = $_GET['subject_id'];
		$result = $this->Subject->getSubjectById($id);
		$data['subject'] = $result;
		$this->load->view('admin/main/main',$data);
	}

	public function updateSubject(){
		$subject = array();

		$subject['subject_id'] = $_POST['subject_id'];
		$subject['subject_code'] = $_POST['subject_code'];
		$subject['subject_name'] = $_POST['subject_name'];
		$subject['subject_des'] = $_POST['subject_des'];

		$this->Subject->updateSubject($_GET['subject_id'],$subject);
		$this->getAllDataSubject();
	}

	public function loadViewAddSubject(){
		$data['menu'] = 'Quản Lí Môn Học';
		$data['title'] = 'Danh Sách Môn Học';
		$data['link_add'] = 'http://localhost/CodeIgniter_Manage_Project/admin/ManageSubject/loadViewAddSubject';
		$data['status_menu'] =  array("exp","active exp","exp","exp","exp");
		$data['data'] = 'admin/manage_subject/AddForm';
		$this->load->view('admin/main/main',$data);
	}

	public function addSubject(){
		$subject = array();
		$subject['subject_code'] = $_POST['subject_code'];
		$subject['subject_name'] = $_POST['subject_name'];
		$subject['subject_des'] = $_POST['subject_des'];

		$this->Subject->addSubject($subject);
		$this->getAllDataSubject();
	}
} 
?>