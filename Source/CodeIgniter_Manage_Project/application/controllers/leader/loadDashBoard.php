<?php
class LoadDashBoard extends CI_Controller{
	public function __construct(){
		parent::__construct();
	}
	
	public function LoadDashBoard(){
		$status_menu = array("active exp","exp");
		$data['menu'] = "DashBoard"; // truyền title của menu.
		$data['status_menu'] = $status_menu;
		$data['link_add'] = '#';
		$data['data'] = '';
		$this->load->view('leader/DashBoard',$data);
	}
}
?>