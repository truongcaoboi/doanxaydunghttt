<?php
class ManageSmallTask extends CI_Controller{

	function __construct(){
		parent::__construct();
		$this->load->library("session");
		$this->load->model('BigTask');
		$this->load->model('Subject');
		$this->load->model('Student');
		$this->load->model('SmallTask');
		$this->load->model('Project');
	}

	public function getGeneralDataBigTask(){
		$on_project = $_GET['on_project'];
		$data = array();
		$result = $this->BigTask->getGeneralDataBigTask($on_project);

		$dataSmallTask = "";

		foreach ($result as $key => $rows_result){
			$dataSmallTask = $dataSmallTask."<tr><td><input type='checkbox' name='big_task_id' value='".$rows_result['big_task_id']."' 
			</td>";
			$dataSmallTask = $dataSmallTask."<td>".$rows_result['big_task_id']."</td>";
			$dataSmallTask = $dataSmallTask."<td>".$_GET['on_project']."</td>";
			$dataSmallTask = $dataSmallTask."<td>".$rows_result['big_task_des']."</td>";
			$dataSmallTask = $dataSmallTask."<td>".$rows_result['big_task_deadline']."</td>";
			$dataSmallTask = $dataSmallTask."<td><a href='http://localhost/CodeIgniter_Manage_Project/public/upload/".$rows_result['file_url']."'>".$rows_result['file_url']."</a></td>";
			$dataSmallTask = $dataSmallTask."<td class='option'> 
						   <a href='http://localhost/CodeIgniter_Manage_Project/member/ManageSmallTask/getAllInformationSmallTask?big_task_id=".$rows_result['big_task_id']."&project_id=".$on_project."'> Chia Task </a></td></tr>";
		}

		$subjects = $this->Student->getSubjectStudentStudy($this->session->userdata("student_id"));
		//tao submenu thong tin Project
			$subMenuSubjectInformation = "<ul class='sub'>";
			foreach ($subjects as $key => $subject) {
				$subMenuSubjectInformation .= "<li><a href='http://localhost/CodeIgniter_Manage_Project/member/ManageSmallTask/getAllInformationProjectById?project_id=".$subject['project_id']."'>".$subject['subject_id']."</a></li>";
				}
			$subMenuSubjectInformation .= "</ul>";
		//tao submenu quan li task project
			$subMenuSubject = "<ul class='sub'>";
			foreach ($subjects as $key => $subject) {
				$subMenuSubject .= "<li><a href='http://localhost/CodeIgniter_Manage_Project/member/ManageSmallTask/getGeneralDataBigTask?on_project=".$subject['project_id']."'>".$subject['subject_id']."</a></li>";
				}
			$subMenuSubject .= "</ul>";
		$data['subMenuSubject'] = $subMenuSubject;
		$data['subMenuSubjectInformation'] = $subMenuSubjectInformation;
		$data['menu'] = "Quản Lí Đồ Án";
		$data['title'] = 'Danh Sách Task Lớn';
		$data['status_menu'] =  array("exp","active exp","exp");
		$data['data'] = 'member/LoadDataBigTask';
		$data['link_add'] = 'http://localhost/CodeIgniter_Manage_Project/member/ManageProject/loadViewAddProject';
		$data['generalDataBigTask'] = $dataSmallTask;
		$this->load->view('member/main/main',$data);
	}

	public function getAllInformationSmallTask(){
		$big_task_id = $_GET['big_task_id'];
		$project_id = $_GET['project_id'];
		$data = array();
		$result = $this->SmallTask->getGeneralDataSmallTask($big_task_id,$project_id);

		$dataSmallTask = "";

		foreach ($result as $key => $rows_result){
			$dataSmallTask = $dataSmallTask."<tr><td><input type='checkbox' name='small_task_id' value='".$rows_result['big_task_id']."' 
			</td>";
			$dataSmallTask = $dataSmallTask."<td>".$rows_result['small_task_id']."</td>";
			$dataSmallTask = $dataSmallTask."<td>".$_GET['big_task_id']."</td>";
			$dataSmallTask = $dataSmallTask."<td>".$rows_result['small_task_des']."</td>";
			$dataSmallTask = $dataSmallTask."<td>".$rows_result['small_task_deadline']."</td>";
			$dataSmallTask = $dataSmallTask."<td>".$rows_result['status']."</td>";
			$student_name  = $this->Student->getStudentNameById($rows_result['student_id']);
			$dataSmallTask = $dataSmallTask."<td>".$student_name[0]['student_name']."</td>";
			$dataSmallTask = $dataSmallTask."<td class='option'> 
						   <a href='http://localhost/CodeIgniter_Manage_Project/member/ManageSmallTask/loadViewUpdateSmallTask?small_task_id=".$rows_result['small_task_id']."&big_task_id=".$big_task_id."&project_id=".$project_id." '>
						   <img src='http://localhost/CodeIgniter_Manage_Project/public/admin_dashboard/images/icons/color/edit.png'/></a>
						   <a href='http://localhost/CodeIgniter_Manage_Project/member/ManageSmallTask/deleteSmallTask?small_task_id=".$rows_result['small_task_id']."&big_task_id=".$big_task_id."&project_id=".$project_id."'> <img src='http://localhost/CodeIgniter_Manage_Project/public/admin_dashboard/images/icons/color/delete.png'/></a></td>";
			$dataSmallTask = $dataSmallTask."<td></td></tr>";		
		}

		
		
		$data = $this->SmallTask->variableStatic($big_task_id,$project_id);
		$subjects = $this->Student->getSubjectStudentStudy($this->session->userdata("student_id"));
		//tao submenu thong tin Project
				
			$subMenuSubjectInformation = "<ul class='sub'>";
			foreach ($subjects as $key => $subject) {
				$subMenuSubjectInformation .= "<li><a href='http://localhost/CodeIgniter_Manage_Project/member/ManageSmallTask/getAllInformationProjectById?project_id=".$subject['project_id']."'>".$subject['subject_id']."</a></li>";
				}
			$subMenuSubjectInformation .= "</ul>";
				//tao submenu quan li task project
			$subMenuSubject = "<ul class='sub'>";
			foreach ($subjects as $key => $subject) {
				$subMenuSubject .= "<li><a href='http://localhost/CodeIgniter_Manage_Project/member/ManageSmallTask/getGeneralDataBigTask?on_project=".$subject['project_id']."'>".$subject['subject_id']."</a></li>";
				}
			$subMenuSubject .= "</ul>";
		$data['subMenuSubject'] = $subMenuSubject;
		$data['subMenuSubjectInformation'] = $subMenuSubjectInformation;
		$data['data'] = 'member/manage_small_task/LoadData';
		$data['generalDataSmallTask'] = $dataSmallTask;
		$this->load->view('member/main/main',$data);
	}

	

	
	public function loadViewUpdateSmallTask(){
		$big_task_id = $_GET['big_task_id'];
		$project_id = $_GET['project_id'];
		$data = array();
		$data = $this->SmallTask->variableStatic($big_task_id,$project_id);
		$subjects = $this->Student->getSubjectStudentStudy($this->session->userdata("student_id"));
			$subMenuSubjectInformation = "<ul class='sub'>";
			foreach ($subjects as $key => $subject) {
				$subMenuSubjectInformation .= "<li><a href='http://localhost/CodeIgniter_Manage_Project/member/ManageSmallTask/getAllInformationProjectById?project_id=".$subject['project_id']."'>".$subject['subject_id']."</a></li>";
				}
			$subMenuSubjectInformation .= "</ul>";
				//tao submenu quan li task project
			$subMenuSubject = "<ul class='sub'>";
			foreach ($subjects as $key => $subject) {
				$subMenuSubject .= "<li><a href='http://localhost/CodeIgniter_Manage_Project/member/ManageSmallTask/getGeneralDataBigTask?on_project=".$subject['project_id']."'>".$subject['subject_id']."</a></li>";
				}
			$subMenuSubject .= "</ul>";
		$data['subMenuSubject'] = $subMenuSubject;
		$data['subMenuSubjectInformation'] = $subMenuSubjectInformation;
		$data['data'] = 'member/manage_small_task/UpdateForm';
		$data['big_task_id'] = $big_task_id;	
		$this->load->view('member/main/main',$data);
	}

	public function updateSmallTask(){
		$smallTask = array();
		$smallTask['status'] = $_POST['status'];

		$this->SmallTask->updateSmallTask($_GET['small_task_id'],$smallTask);
		$this->getAllInformationSmallTask();
	}

		public function deleteSmallTask(){
		$this->SmallTask->deleteSmallTask($_GET['small_task_id']);
		$this->getAllInformationSmallTask();
	}

	public function getAllInformationProjectById(){
		$project_id = $_GET['project_id'];
		$data['menu'] = "Thông Tin Đồ Án";
		$data['title'] = 'Thông Tin Đồ Án';
		$data['data'] = 'member/manage_small_task/LoadDataProject';
		$data['status_menu'] = array("exp","active exp","exp");
		$data['link_add'] = '#';
		$data['generalDataProject'] = $this->Project->getAllInformationProjectById($project_id);//thong tin cua Project
		$data['information_student'] = $this->Student->getStudentOnProject($project_id);//danh sach sinh vein thuoc Project
		$subjects = $this->Student->getSubjectStudentStudy($this->session->userdata("student_id"));
			$subMenuSubjectInformation = "<ul class='sub'>";
			foreach ($subjects as $key => $subject) {
				$subMenuSubjectInformation .= "<li><a href='http://localhost/CodeIgniter_Manage_Project/member/ManageSmallTask/getAllInformationProjectById?project_id=".$subject['project_id']."'>".$subject['subject_id']."</a></li>";
				}
			$subMenuSubjectInformation .= "</ul>";
				//tao submenu quan li task project
			$subMenuSubject = "<ul class='sub'>";
			foreach ($subjects as $key => $subject) {
				$subMenuSubject .= "<li><a href='http://localhost/CodeIgniter_Manage_Project/member/ManageSmallTask/getGeneralDataBigTask?on_project=".$subject['project_id']."'>".$subject['subject_id']."</a></li>";
				}
			$subMenuSubject .= "</ul>";
		$data['subMenuSubject'] = $subMenuSubject;
		$data['subMenuSubjectInformation'] = $subMenuSubjectInformation;
		$this->load->view('member/main/main',$data);
	}

	public function Test(){
		$result = $this->SmallTask->getGeneralDataSmallTask(17,19);
			$student_name  = $this->Student->getStudentNameById($result[0]['student_id']);
			print_r($result);
			echo "<br>";
			print_r($result[0]);
			print_r($student_name);

			
			

	}
}
?>