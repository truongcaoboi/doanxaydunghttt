<?php
class Login extends CI_Controller{
	public function __construct(){
		parent::__construct();

		$this->load->library("session");
		$this->load->model('UserLogin');
		$this->load->model('Subject');
		$this->load->model('Student');
	}

	public function index(){
		$this->load->view('login/index.php');
	}

	public function login(){
		$userName = $this->input->post('user_name');
		$passWord = $this->input->post('pass_word');

		$validateUser = $this->UserLogin->validate_user($userName,$passWord);
		$data = array();
		
		if($validateUser['mess']==true){
			if($validateUser['user_type']==0){
				$status_menu = array("active exp","exp","exp","exp","exp");
				$data['menu'] = "DashBoard"; // truyền title của menu.
				$data['status_menu'] = $status_menu;
				$data['data'] = $validateUser['user_type'];
				$this->load->view('admin/DashBoard',$data);
			}
			///leader
			elseif($validateUser['user_type']==1){
				$this->session->set_userdata("student_id",$userName);//tao session
				//tao submenu thong tin Project
				$subjects = $this->Student->getSubjectStudentStudy($userName);
				$subMenuSubjectInformation = "<ul class='sub'>";
				foreach ($subjects as $key => $subject) {
					$subMenuSubjectInformation .= "<li><a href='http://localhost/CodeIgniter_Manage_Project/leader/ManageSmallTask/getAllInformationProjectById?project_id=".$subject['project_id']."'>".$subject['subject_id']."</a></li>";
				}
				$subMenuSubjectInformation .= "</ul>";
				//tao submenu quan li task project
				$subMenuSubject = "<ul class='sub'>";
				foreach ($subjects as $key => $subject) {
					$subMenuSubject .= "<li><a href='http://localhost/CodeIgniter_Manage_Project/leader/ManageSmallTask/getGeneralDataBigTask?on_project=".$subject['project_id']."'>".$subject['subject_id']."</a></li>";
				}
				$subMenuSubject .= "</ul>";
				$status_menu = array("active exp","exp","exp");//hieu ung cho thanh menu
				$data['menu'] = "DashBoard"; // truyền title của menu.
				$data['subMenuSubject'] = $subMenuSubject;
				$data['subMenuSubjectInformation'] = $subMenuSubjectInformation;
				$data['status_menu'] = $status_menu;
				$data['data'] = $validateUser['user_type'];
				$this->load->view('leader/DashBoard',$data);	
			}
			//member
			elseif ($validateUser['user_type']==2) {
				$this->session->set_userdata("student_id",$userName);//tao session
				//tao submenu thong tin Project
				$subjects = $this->Student->getSubjectStudentStudy($userName);
				$subMenuSubjectInformation = "<ul class='sub'>";
				foreach ($subjects as $key => $subject) {
					$subMenuSubjectInformation .= "<li><a href='http://localhost/CodeIgniter_Manage_Project/member/ManageSmallTask/getAllInformationProjectById?project_id=".$subject['project_id']."&'>".$subject['subject_id']."</a></li>";
				}
				$subMenuSubjectInformation .= "</ul>";
				//tao submenu quan li task project
				$subMenuSubject = "<ul class='sub'>";
				foreach ($subjects as $key => $subject) {
					$subMenuSubject .= "<li><a href='http://localhost/CodeIgniter_Manage_Project/member/ManageSmallTask/getGeneralDataBigTask?on_project=".$subject['project_id']."'>".$subject['subject_id']."</a></li>";
				}
				$subMenuSubject .= "</ul>";
				$status_menu = array("active exp","exp","exp");//hieu ung cho thanh menu
				$data['menu'] = "DashBoard"; // truyền title của menu.
				$data['subMenuSubject'] = $subMenuSubject;
				$data['subMenuSubjectInformation'] = $subMenuSubjectInformation;
				$data['status_menu'] = $status_menu;
				$data['data'] = $validateUser['user_type'];
				$this->load->view('member/DashBoard',$data);
			}
		} else{
			$this->load->view('login/LoginFailed.php');
		}
	}
}
?>