<form class="form" id="form" action="http://localhost/CodeIgniter_Manage_Project/admin/ManageBigTask/addBigTask?on_project=<?php echo $on_project;?>" method="post" enctype="multipart/form-data">
			<fieldset>
				<div class="widget">
				    <div class="title">
						<h6>Thêm Task Lớn</h6>
					</div>
					
					<div class="tab_container">
					     <div id='tab1' class="tab_content pd0">
 
<div class="formRow">
	<label class="formLeft" for="big_task_des">Mô tả<span class="req">*</span></label>
	<div class="formRight">
		<span class="oneTwo"><input name="big_task_des" id="big_task_des" _autocheck="true" type="text" /></span>
		<span name="name_autocheck" class="autocheck"></span>
		<div name="name_error" class="clear error"></div>
	</div>
	<div class="clear"></div>
</div>

<input type="hidden" name="on_project" value="<?php echo $on_project;?>">
<div class="formRow">
	<label class="formLeft" for="big_task_deadline">Deadline<span class="req">*</span></label>
	<div class="formRight">
		<span class="oneTwo"><input name="big_task_deadline" id="big_task_deadline" _autocheck="true" type="date" /></span>
		<span name="name_autocheck" class="autocheck"></span>
		<div name="name_error" class="clear error"></div>
	</div>
	<div class="clear"></div>
</div>

<div class="formRow">
	<label class="formLeft" for="file_url">File Upload<span class="req">*</span></label>
	<div class="formRight">
		<span class="oneTwo"><input name="file_url" id="file_url" _autocheck="true" type="file" /></span>
		<span name="name_autocheck" class="autocheck"></span>
		<div name="name_error" class="clear error"></div>
	</div>
	<div class="clear"></div>
</div>


						<div class="formRow hide"></div>
						 </div>
						 						
	        		</div><!-- End tab_container-->
	        		
	        		<div class="formSubmit">
	           			<input type="submit" value="Cập Nhật" class="redB" />
	           			<input type="reset" value="Hủy Bỏ" class="basic" />
	           		</div>
	        		<div class="clear"></div>
				</div>
			</fieldset>
		</form>