<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html" charset="utf-8" />

<title>Application v1.0</title>

<meta name="robots" content="noindex, nofollow" />

<link rel="shortcut icon" href="http://localhost/CodeIgniter_Manage_Project/public/admin_dashboard/images/icon.png" type="image/x-icon"/>
<link rel="stylesheet" type="text/css" href="http://localhost/CodeIgniter_Manage_Project/public/admin_dashboard/crown/css/main.css" />
<link rel="stylesheet" type="text/css" href="http://localhost/CodeIgniter_Manage_Project/public/admin_dashboard/css/css.css" media="screen" />

<link rel="stylesheet" type="text/css" href="../js/jquery/colorbox/colorbox.css" media="screen" />

<script type="text/javascript" src="../js/custom_admin.js" type="text/javascript"></script>
</head>
<body>
	<!-- Left side content -->
	<div id="left_content">
		<?php $this->load->view('admin/main/LeftContent',$data); ?>
	</div>	

	<div id="rightSide">
		<!-- Account panel top -->
		<div class="topNav">
			<?php $this->load->view('admin/main/PanelTop'); ?>
		</div>
		<!-- Title area -->
		<div class="titleArea">
			<?php $this->load->view('admin/main/TitleArea',$data); ?>
		</div>
		<div class="line"></div>
		<!-- Main Content -->
		<div class="wrapper">
				<?php $this->load->view($data); ?>
		</div>
		<div class="clear mt30"></div>
		<div id="footer">
			<?php $this->load->view('admin/main/Footer'); ?>
		</div>
		<div class="clear"></div>
</body>
</html>
