<form class="form" id="form" action="http://localhost/CodeIgniter_Manage_Project/admin/ManageSubject/addSubject" method="post" enctype="multipart/form-data">
			<fieldset>
				<div class="widget">
				    <div class="title">
						<h6>Thêm Môn Học</h6>
					</div>
					
					<div class="tab_container">
					     <div id='tab1' class="tab_content pd0">
					     	<div class="formRow">
								<label class="formLeft" for="subject_code">Mã môn học:<span class="req">*</span></label>
								<div class="formRight">
									<span class="oneTwo"><input name="subject_code" id="subject_code" _autocheck="true" type="text" /></span>
									<span name="name_autocheck" class="autocheck"></span>
									<div name="name_error" class="clear error"></div>
								</div>
								<div class="clear"></div>
							</div>

							<div class="formRow">
								<label class="formLeft" for="subject_name">Tên Môn Học:<span class="req">*</span></label>
								<div class="formRight">
									<span class="oneTwo"><input name="subject_name" id="subject_name" _autocheck="true" type="text" /></span>
									<span name="name_autocheck" class="autocheck"></span>
									<div name="name_error" class="clear error"></div>
								</div>
								<div class="clear"></div>
							</div>
						     			
							<div class="formRow">
								<label class="formLeft" for="subject_des">Mô tả</label>
								<div class="formRight">
									<span class="oneTwo"><textarea name="subject_des" id="subject_des" _autocheck="true" rows="4" cols=""></textarea></span>
									<span name="site_title_autocheck" class="autocheck"></span>
									<div name="site_title_error" class="clear error"></div>
								</div>
								<div class="clear"></div>
							</div>

						    <div class="formRow hide"></div>
						 </div>
						 						
	        		</div><!-- End tab_container-->
	        		
	        		<div class="formSubmit">
	           			<input type="submit" value="Thêm" class="redB" />
	           			<input type="reset" value="Hủy Bỏ" class="basic" />
	           		</div>
	        		<div class="clear"></div>
				</div>
			</fieldset>
		</form>