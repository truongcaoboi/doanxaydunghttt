<!DOCTYPE html>
<html>
<head>
	<title>Yeu That Long Ok</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" type="text/css" href="http://localhost/CodeIgniter_Manage_Project/public/student_css/find_student_style.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
</head>
<body>
<form class="form" id="form" action="http://localhost/CodeIgniter_Manage_Project/admin/ManageProject/addProject" method="post" enctype="multipart/form-data">
			<fieldset>
				<div class="widget">
				    <div class="title">
						<h6>Thêm Project</h6>
					</div>
					
					<div class="tab_container">
					     <div id='tab1' class="tab_content pd0">

<div class="formRow">
	<label class="formLeft" for="project_id_add">Mã Nhóm:<span class="req">*</span></label>
	<div class="formRight">
		<span class="oneTwo"><input name="project_code" id="project_id_add" onkeyup="checkIdProject()" _autocheck="true" type="text" />
		<div id="checkIdProject"></div>
		</span>
		<span name="name_autocheck" class="autocheck"></span>
		<div name="name_error" class="clear error"></div>
	</div>
	<div class="clear"></div>
</div>

 <div class="formRow">
	<label class="formLeft" for="project_name">Tên Nhóm:<span class="req">*</span></label>
	<div class="formRight">
		<span class="oneTwo"><input name="project_name" id="project_name" _autocheck="true" type="text" /></span>
		<span name="name_autocheck" class="autocheck"></span>
		<div name="name_error" class="clear error"></div>
	</div>
	<div class="clear"></div>
</div>

<!-- Price -->
<div class="formRow">
	<label class="formLeft" for="param_cat">Thuộc Môn Học<span class="req">*</span></label>
	<div class="formRight">
		<select name="subject_id" _autocheck="true" id='subject_id' class="left">
					<?php foreach ($subjects as $key => $subject) { 
						echo "<option value='".$subject['subject_id']."'>".$subject['subject_name']."</option>";			
					}
					 ?>
		</select>
		<span name="cat_autocheck" class="autocheck"></span>
		<div name="cat_error" class="clear error"></div>
	</div>
	<div class="clear"></div>
</div>

<div class="formRow">
	<label class="formLeft" for="param_cat">Danh Sách Sinh Viên<span class="req">*</span></label>
	<div class="formRight">
		<input type="text" id="add_student" onkeyup="addStudent()" placeholder="Search for names.." title="Type in a name"/>
	<div id = "data"></div>
	<div id="display"></div>
	<script type="text/javascript">
		function addStudent(){			
				var filter,hint;
				filter = document.getElementById('add_student').value;
				$.getJSON("http://localhost/CodeIgniter_Manage_Project/admin/ManageProject/getHintStudent?hint="+filter, 
					function(result) {
					var dataReturn="";
					var i;
					dataReturn += "<ul id='myUL'>";
    				for(i = 0; i < result.minhtu.length; i++ ){
    					dataReturn += "<li><a href='#' data-value='" + result.minhtu[i]['human_code'] + "'>"; 
    					dataReturn += result.minhtu[i]['human_name'];
    					dataReturn += "</a></li>";
    				}
					dataReturn += "</ul>";
					document.getElementById("display").innerHTML = dataReturn;

					$(document).ready(function(){
						$('a').click(function(){
							var i = $(this).data("value");
							var queryString;
							queryString = "student_id=" + i + "&project_id="+$('#project_id_add').val();
							jQuery.ajax({
								url: "http://localhost/CodeIgniter_Manage_Project/admin/ManageProject/addStudentToProject",
								data: queryString,
								type: "GET",
							});
							$('#data').append("<div>"+ $(this).text() + "<img src='http://localhost/CodeIgniter_Manage_Project/public/admin_dashboard/images/icons/color/delete.png'/></div><br>");
							$(this).hide();
							$('#add_student').val('');
						})
					})		
				});
			}

		function checkIdProject(){
			var input,filter;
			input = document.getElementById('project_id_add');
			filter = input.value;
			if (filter == ""){
				document.getElementById("checkIdProject").innerHTML = "vui long dien id project";
			} else{
			$.get("http://localhost/CodeIgniter_Manage_Project/admin/ManageProject/checkIdProject?project_id="+filter, 
				function($message){
				document.getElementById("checkIdProject").innerHTML = $message;
			});
			}
		}
	</script>
		<span name="cat_autocheck" class="autocheck"></span>
		<div name="cat_error" class="clear error"></div>
	</div>
	<div class="clear"></div>
</div>


<div class="formRow">
	<label class="formLeft" for="topic_name">Tên Đề Tài:</label>
	<div class="formRight">
		<span class="oneTwo"><textarea name="topic_name" id="topic_name" rows="4" cols=""></textarea></span>
		<span name="sale_autocheck" class="autocheck"></span>
		<div name="sale_error" class="clear error"></div>
	</div>
	<div class="clear"></div>
</div>					         <div class="formRow hide"></div>
						 </div>
						 
						 <div id='tab2' class="tab_content pd0" >
						     			
<div class="formRow">
	<label class="formLeft" for="topic_des">Mô tả</label>
	<div class="formRight">
		<span class="oneTwo"><textarea name="topic_des" id="topic_des" _autocheck="true" rows="4" cols=""></textarea></span>
		<span name="site_title_autocheck" class="autocheck"></span>
		<div name="site_title_error" class="clear error"></div>
	</div>
	<div class="clear"></div>
</div>
						     			
<div class="formRow">
	<label class="formLeft" for="#">Tai lieu dinh kem</label>
	<div class="formRight">
		<span class="oneTwo"><input type="file" name=""> name="#" id="#" _autocheck="true" rows="4" cols=""></textarea></span>
		<span name="site_title_autocheck" class="autocheck"></span>
		<div name="site_title_error" class="clear error"></div>
	</div>
	<div class="clear"></div>
</div>

						     <div class="formRow hide"></div>
						 </div>
						 						
	        		</div><!-- End tab_container-->
	        		
	        		<div class="formSubmit">
	           			<input type="submit" value="Cập Nhật" class="redB" />
	           			<input type="reset" value="Hủy Bỏ" class="basic" />
	           		</div>
	        		<div class="clear"></div>
				</div>
			</fieldset>
		</form>
</body>
</html>