<form class="form" id="form" action="http://localhost/CodeIgniter_Manage_Project/admin/ManageStudent/updateStudent?student_id=<?php echo $_GET['student_id']; ?>" method="post" enctype="multipart/form-data">
			<fieldset>
				<div class="widget">
				    <div class="title">
						<h6>Thêm Sinh Viên</h6>
					</div>
					
					<div class="tab_container">
					     <div id='tab1' class="tab_content pd0">
 
<div class="formRow">
	<label class="formLeft" for="student_id">Mã Số Sinh Viên<span class="req">*</span></label>
	<div class="formRight">
		<span class="oneTwo"><input name="student_code" id="student_id" _autocheck="true" type="text" value="<?php echo $student['student_code'] ?>" /></span>
		<span name="name_autocheck" class="autocheck"></span>
		<div name="name_error" class="clear error"></div>
	</div>
	<div class="clear"></div>
</div>

<div class="formRow">
	<label class="formLeft" for="student_name">Tên Sinh Viên<span class="req">*</span></label>
	<div class="formRight">
		<span class="oneTwo"><input name="student_name" id="student_name" _autocheck="true" type="text" value="<?php echo $student['student_name'] ?>"/></span>
		<span name="name_autocheck" class="autocheck"></span>
		<div name="name_error" class="clear error"></div>
	</div>
	<div class="clear"></div>
</div>

<div class="formRow">
	<label class="formLeft" for="student_course">Khóa học<span class="req">*</span></label>
	<div class="formRight">
		<span class="oneTwo"><input name="student_course" id="student_course" _autocheck="true" type="text" value="<?php echo $student['student_course'] ?>"/></span>
		<span name="name_autocheck" class="autocheck"></span>
		<div name="name_error" class="clear error"></div>
	</div>
	<div class="clear"></div>
</div>

<div class="formRow">
	<label class="formLeft" for="student_class">Lớp<span class="req">*</span></label>
	<div class="formRight">
		<span class="oneTwo"><input name="student_class" id="student_class" _autocheck="true" type="text" value="<?php echo $student['student_class'] ?>"/></span>
		<span name="name_autocheck" class="autocheck"></span>
		<div name="name_error" class="clear error"></div>
	</div>
	<div class="clear"></div>
</div>

<script type="text/javascript">
	function showProject(str){
		if(str==""){
			document.getElementById("txtHint").innerHTML = "";
			return;
		} else{
			xmlhttp = new XMLHttpRequest();
			xmlhttp.onreadystatechange = function(){
				if(this.readyState == 4 && this.status == 200){
					document.getElementById("txtHint").innerHTML = this.responseText;
				}
			}
		};
		xmlhttp.open("GET","http://localhost/CodeIgniter_Manage_Project/admin/ManageProject/getProjectBySubject/"+str,true);
		xmlhttp.send();
	}
</script>

<!-- Price -->
<div class="formRow">
	<label class="formLeft" for="param_cat">Thuộc Môn Học<span class="req">*</span></label>
	<div class="formRight">
		<select name="subject_id" _autocheck="true" id='subject_id' class="left" onchange="showProject(this.value)">
					<?php foreach ($subjects as $key => $subject) { 
						echo "<option value='".$subject['subject_id']."'>".$subject['subject_name']."</option>";			
					}
					?>
		</select>
		<span name="cat_autocheck" class="autocheck"></span>
		<div name="cat_error" class="clear error"></div>
	</div>
	<div class="clear"></div>
</div>

<div class="formRow">
	<label class="formLeft" for="param_cat">Thuộc Project<span class="req">*</span></label>
	<div class="formRight">
			<div id="txtHint"></div>
		</select>	
		<span name="cat_autocheck" class="autocheck"></span>
		<div name="cat_error" class="clear error"></div>
	</div>
	<div class="clear"></div>
</div>

<div class="formRow">
	<label class="formLeft" for="email">Email<span class="req">*</span></label>
	<div class="formRight">
		<span class="oneTwo"><input name="email" id="email" _autocheck="true" type="text" value="<?php echo $student['student_email'] ?>"/></span>
		<span name="name_autocheck" class="autocheck"></span>
		<div name="name_error" class="clear error"></div>
	</div>
	<div class="clear"></div>
</div>

<div class="formRow">
	<label class="formLeft" for="facebook">Facebook<span class="req">*</span></label>
	<div class="formRight">
		<span class="oneTwo"><input name="facebook" id="facebook" _autocheck="true" type="text" value="<?php echo $student['student_facebook'] ?>"/></span>
		<span name="name_autocheck" class="autocheck"></span>
		<div name="name_error" class="clear error"></div>
	</div>
	<div class="clear"></div>
</div>

<div class="formRow">
	<label class="formLeft" for="phone">Phone<span class="req">*</span></label>
	<div class="formRight">
		<span class="oneTwo"><input name="phone" id="phone" _autocheck="true" type="text" value="<?php echo $student['student_phone'] ?>"/></span>
		<span name="name_autocheck" class="autocheck"></span>
		<div name="name_error" class="clear error"></div>
	</div>
	<div class="clear"></div>
</div>



						<div class="formRow hide"></div>
						 </div>
						 						
	        		</div><!-- End tab_container-->
	        		
	        		<div class="formSubmit">
	           			<input type="submit" value="Cập Nhật" class="redB" />
	           			<input type="reset" value="Hủy Bỏ" class="basic" />
	           		</div>
	        		<div class="clear"></div>
				</div>
			</fieldset>
		</form>