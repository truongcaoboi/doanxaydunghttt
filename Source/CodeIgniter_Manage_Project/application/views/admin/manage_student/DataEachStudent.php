<html>
<head>
	<meta name="viewport" content="width=device-width, initial-scale=1"/>
	<link rel="stylesheet" type="text/css" href="http://localhost/CodeIgniter_Manage_student/public/student_css/find_student_style.css"/>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
</head>
<body>
			<fieldset>
				<div class="widget">
				    <div class="title">
						<h6>Thông tin Sinh Viên</h6>
					</div>
					
					<div class="tab_container">
					     <div id='tab1' class="tab_content pd0">
 <div class="formRow">
	<label class="formLeft" for="student_name">Tên Sinh Viên:<span class="req">*</span></label>
	<div class="formRight">
		<span class="oneTwo"><?php echo $student['student_name']?></span>
		<span name="name_autocheck" class="autocheck"></span>
		<div name="name_error" class="clear error"></div>
	</div>
	<div class="clear"></div>
</div>

<!-- Price -->
<div class="formRow">
	<label class="formLeft" for="param_cat">Khóa Học<span class="req">*</span></label>
	<div class="formRight">
		<span class="oneTwo"><?php echo $student['student_course']?></span>
		<span name="cat_autocheck" class="autocheck"></span>
		<div name="cat_error" class="clear error"></div>
	</div>
	<div class="clear"></div>
</div>

<div class="formRow">
	<label class="formLeft" for="topic_name">Lớp</label>
	<div class="formRight">
		<span class="oneTwo">
			<?php echo $student['student_class']?>
		</span>
		<span name="sale_autocheck" class="autocheck"></span>
		<div name="sale_error" class="clear error"></div>
	</div>
	<div class="clear"></div>
</div>					         <div class="formRow hide"></div>
						 </div>
						 
						 <div id='tab2' class="tab_content pd0" >

<div class="formRow">
	<label class="formLeft" for="topic_des">Thuộc Project</label>
	<div class="formRight">
		<span class="oneTwo">
			<?php foreach ($subject_study as $key => $project){
				echo $project['project_name']."-".$project['subject_id'];
				echo "<br>";
			}
			?>
		</span>
		<span name="site_title_autocheck" class="autocheck"></span>
		<div name="site_title_error" class="clear error"></div>
	</div>
	<div class="clear"></div>
</div>
				     			
<div class="formRow">
	<label class="formLeft" for="topic_des">Email</label>
	<div class="formRight">
		<span class="oneTwo">
			<?php echo $student['student_email']; ?>
		</span>
		<span name="site_title_autocheck" class="autocheck"></span>
		<div name="site_title_error" class="clear error"></div>
	</div>
	<div class="clear"></div>
</div>

<div class="formRow">
	<label class="formLeft" for="topic_des">Facebook</label>
	<div class="formRight">
		<span class="oneTwo">
			<?php echo $student['student_facebook']; ?>
		</span>
		<span name="site_title_autocheck" class="autocheck"></span>
		<div name="site_title_error" class="clear error"></div>
	</div>
	<div class="clear"></div>
</div>

<div class="formRow">
	<label class="formLeft" for="topic_des">Phone</label>
	<div class="formRight">
		<span class="oneTwo">
			<?php echo $student['student_phone']; ?>
		</span>
		<span name="site_title_autocheck" class="autocheck"></span>
		<div name="site_title_error" class="clear error"></div>
	</div>
	<div class="clear"></div>
</div>



						     <div class="formRow hide"></div>
						 </div>
				</div>		 						
	        		</div><!-- End tab_container-->
			</fieldset>
</body>
</html>