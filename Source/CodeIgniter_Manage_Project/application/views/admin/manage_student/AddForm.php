<form class="form" id="form" action="http://localhost/CodeIgniter_Manage_Project/admin/ManageStudent/addStudent" method="post" enctype="multipart/form-data">
			<fieldset>
				<div class="widget">
				    <div class="title">
						<h6>Thêm Sinh Viên</h6>
					</div>
					
					<div class="tab_container">
					     <div id='tab1' class="tab_content pd0">
 
<div class="formRow">
	<label class="formLeft" for="student_id">Mã Số Sinh Viên<span class="req">*</span></label>
	<div class="formRight">
		<span class="oneTwo"><input name="student_code" id="student_id" _autocheck="true" type="text" /></span>
		<span name="name_autocheck" class="autocheck"></span>
		<div name="name_error" class="clear error"></div>
	</div>
	<div class="clear"></div>
</div>

<div class="formRow">
	<label class="formLeft" for="student_name">Tên Sinh Viên<span class="req">*</span></label>
	<div class="formRight">
		<span class="oneTwo"><input name="student_name" id="student_name" _autocheck="true" type="text" /></span>
		<span name="name_autocheck" class="autocheck"></span>
		<div name="name_error" class="clear error"></div>
	</div>
	<div class="clear"></div>
</div>

<div class="formRow">
	<label class="formLeft" for="student_course">Khóa học<span class="req">*</span></label>
	<div class="formRight">
		<span class="oneTwo"><input name="student_course" id="student_course" _autocheck="true" type="text" /></span>
		<span name="name_autocheck" class="autocheck"></span>
		<div name="name_error" class="clear error"></div>
	</div>
	<div class="clear"></div>
</div>

<div class="formRow">
	<label class="formLeft" for="student_class">Lớp<span class="req">*</span></label>
	<div class="formRight">
		<span class="oneTwo"><input name="student_class" id="student_class" _autocheck="true" type="text" /></span>
		<span name="name_autocheck" class="autocheck"></span>
		<div name="name_error" class="clear error"></div>
	</div>
	<div class="clear"></div>
</div>

<div class="formRow">
	<label class="formLeft" for="email">Email<span class="req">*</span></label>
	<div class="formRight">
		<span class="oneTwo"><input name="email" id="email" _autocheck="true" type="text" /></span>
		<span name="name_autocheck" class="autocheck"></span>
		<div name="name_error" class="clear error"></div>
	</div>
	<div class="clear"></div>
</div>

<div class="formRow">
	<label class="formLeft" for="facebook">Facebook<span class="req">*</span></label>
	<div class="formRight">
		<span class="oneTwo"><input name="facebook" id="facebook" _autocheck="true" type="text" /></span>
		<span name="name_autocheck" class="autocheck"></span>
		<div name="name_error" class="clear error"></div>
	</div>
	<div class="clear"></div>
</div>

<div class="formRow">
	<label class="formLeft" for="phone">Phone<span class="req">*</span></label>
	<div class="formRight">
		<span class="oneTwo"><input name="phone" id="phone" _autocheck="true" type="text" /></span>
		<span name="name_autocheck" class="autocheck"></span>
		<div name="name_error" class="clear error"></div>
	</div>
	<div class="clear"></div>
</div>

<div class="formRow">
	<label class="formLeft" for="user_type">Loại Tài Khoản<span class="req">*</span></label>
	<div class="formRight">
		<select name="user_type" _autocheck="true" id='user_type' class="left">
			<option value="1">Leader</option>
			<option value="2">Member</option>
		</select>
		<span name="name_autocheck" class="autocheck"></span>
		<div name="name_error" class="clear error"></div>
	</div>
	<div class="clear"></div>
</div>

						<div class="formRow hide"></div>
						 </div>
						 						
	        		</div><!-- End tab_container-->
	        		
	        		<div class="formSubmit">
	           			<input type="submit" value="Cập Nhật" class="redB" />
	           			<input type="reset" value="Hủy Bỏ" class="basic" />
	           		</div>
	        		<div class="clear"></div>
				</div>
			</fieldset>
		</form>