<?php
class Project extends CI_Model{
	
	function __construct(){
		$this->load->database();
	}

	public function variableStatic(){
		$data = array();
		$data['menu'] = "Quản Lí Đồ Án";
		$data['title'] = 'Danh Sách Đồ Án';
		$data['status_menu'] =  array("exp","exp","active exp","exp","exp");
		$data['data'] = 'admin/manage_project/LoadData';
		$data['link_add'] = 'http://localhost/CodeIgniter_Manage_Project/admin/ManageProject/loadViewAddProject';
		return $data;
	}

	public function getProjectById($id){
		$query = $this->db->query("SELECT * FROM project WHERE project_id = ".$id);
		$result = $query->result_array();
		foreach ($result as $key => $row_result) {
			$data['project_id'] = $id;
			$data['project_code'] = $row_result['project_code'];
			$data['project_name'] = $row_result['project_name'];
			$data['topic_name'] = $row_result['topic_name'];
			$data['topic_des'] = $row_result['topic_des'];
			$data['teacher_id'] = $row_result['teacher_id'];
			$data['leader_id'] = $row_result['leader_id'];
		}
		return $data;
	}

	public function getGeneralDataProject(){
		$query = $this->db->query("SELECT * FROM project order by subject_id,project_id");
		$result = $query->result_array();
		return $result;
	}

	public function deleteProject($project_id){			
		$this->db->query("DELETE FROM project WHERE project_id='".$project_id."'");
		$this->db->query("DELETE FROM project_student WHERE project_id=".$project_id);
	}

	public function updateProject($project_id,$project){
		$this->db->where("project_id",$project_id);
		$this->db->update("project",$project);
	}
  
	public function addProject($project){ 
		$this->db->insert("project",$project);
	}

	public function getProjectBySubject($subject_id){
		$query = $this->db->query("SELECT project_name,project_id FROM project,subject where 
										  project.subject_id=subject.subject_name and subject.subject_id=".$subject_id);
		$result = $query->result_array();
		return $result;
	}

	public function getMaxIDProject(){
		$query = $this->db->query("SELECT MAX(project_id) FROM project");
		$result = $query->result_array();
		return $result[0]['MAX(project_id)'];
	}

	public function checkIdProject($project_id){
		$query = $this->db->query("SELECT * FROM project WHERE project_id=".$project_id);
		$result = $query->result_array();
		$countElement = count($result);
		if($countElement > 0){
			$message = "ID da bi trung, ban vui long nhap id khac";
		} else{
			$message = "ID hop le";
		}
		return $message;
	}

	public function getAllInformationProjectById($project_id){
		$query = $this->db->query("SELECT * FROM project WHERE project_id=".$project_id);
		$result = $query->result_array();
		return $result;
	}
}
?>