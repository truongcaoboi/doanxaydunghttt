<?php
class Subject extends CI_Model{
	function __construct(){
		$this->load->database();
	}

	public function getAllDataSubject(){
		$query = $this->db->query("SELECT * FROM subject");
		$result = $query->result_array();
		return $result;
	}

	public function getSubjectById($id){
		$query = $this->db->query("SELECT * FROM subject where subject_id = ".$id);
		$result = $query->result_array();
		foreach ($result as $key => $rows_result){
			$data['subject_id'] = $id;
			$data['subject_code'] = $rows_result['subject_code'];
			$data['subject_name'] = $rows_result['subject_name'];
			$data['subject_des'] = $rows_result['subject_des'];
		}
		return $data;
	}

	public function deleteSubject($subject_id){			
		$this->db->query("DELETE FROM subject WHERE subject_id='".$subject_id."'");
	}

	public function updateSubject($subject_id,$subject){
		$this->db->where("subject_id",$subject_id);
		$this->db->update("subject",$subject);
	}

	public function addSubject($subject){
		$this->db->insert("subject",$subject);
	}

	public function getSubjectInformation(){
		$query = $this->db->query("SELECT * FROM subject");
		$result = $query->result_array();
		return $result;
	}

	public function getSubjectNameById($subject_id){
		$query = $this->db->query("SELECT subject_name FROM subject where subject_id=".$subject_id);
		$result = $query->result_array();
		return $result;
	}
}
?>