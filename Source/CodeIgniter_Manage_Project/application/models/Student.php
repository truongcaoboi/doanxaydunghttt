<?php
class Student extends CI_Model{
	function __construct(){
		$this->load->database();
	}

	public function getAllDataStudent(){
		$query = $this->db->query("SELECT * FROM human where human_type = 2");
		$result = $query->result_array();
		return $result;
	}

	public function getStudentById($id){
		$query = $this->db->query("SELECT * FROM human where human_id = ".$id." AND human_type = 2");
		$result = $query->result_array();
		foreach ($result as $key => $row_result) {
			$data['student_id'] = $id;
			$data['student_code'] = $row_result['human_code'];
			$data['student_name'] = $row_result['human_name'];
			$data['student_class'] = $row_result['human_class'];
			$data['student_course'] = $row_result['human_course'];
			$data['student_email'] = $row_result['human_email'];
			$data['student_facebook'] = $row_result['human_facebook'];
			$data['student_phone'] = $row_result['human_phone'];
		}
		return $data;
	}

	public function deleteStudent($student_id){			
		$this->db->query("DELETE FROM human WHERE human_id=".$student_id);
	}

	public function updateStudent($student_id,$student){
		$this->db->where("human_id",$student_id);
		$this->db->update("human",$student);
	}

	public function addStudent($student){
		$this->db->insert("human",$student);
	}

	public function getHintStudent($hint){
		$sql = "SELECT human_code,human_name FROM human WHERE human_code like '%".$hint."%'";
		$query = $this->db->query($sql); 
		$result = $query->result_array();
		return $result;
	}

	public function addStudentToProject($student_project){
		$this->db->insert("project_student",$student_project);
	}

	public function deleteStudentOnProject($project_id){
		$this->db->query("DELETE FROM project_student where project_id=".$project_id);
	}

	public function getStudentOnProject($project_id){
		$query = $this->db->query("SELECT human_name,human.human_id FROM human,project_student WHERE human.human_id=project_student.student_id and project_student.project_id=".$project_id." and human.human_type = 2");
		$result = $query->result_array();
		return $result;
	}

	public function getStudentNameById($student_id){
		$query = $this->db->query("SELECT human_name FROM human WHERE human_id=".$student_id);
		$result = $query->result_array();
		return $result;
	}

	public function getStudentIdOnProject($project_id){
		$query = $this->db->query("SELECT student_id FROM project_student WHERE project_id=".$project_id);
		$result = $query->result_array();
		return $result;
	}

	public function getSubjectStudentStudy($student_id){
		$queryString = "SELECT DISTINCT project.subject_id,project.project_name,project.project_id FROM human,project,project_student WHERE human.human_id=project_student.student_id AND project.project_id=project_student.project_id AND human.human_id=".$student_id;
		$query = $this->db->query($queryString);
		$result = $query->result_array();
		return $result;
	}

	public function addScoreToStudent($project_id,$student_id,$score){
		$queryString = "UPDATE project_student SET score =".$score." WHERE student_id =".$student_id." AND project_id=".$project_id;
		$query = $this->db->query($queryString);
	}

	public function getScoreStudent($project_id){
		$queryString = "SELECT student.student_name, project_student.student_id,score FROM project_student,student WHERE student.student_id = project_student.student_id and project_id=".$project_id;
		$query = $this->db->query($queryString);
		$result = $query->result_array();
		return $result; 
	}

	public function createUser($user){
		$this->db->insert("user",$user);
	}
}