<?php
class SmallTask extends CI_Model{
	function __construct(){
		$this->load->database();
	}

	public function variableStatic($big_task_id,$project_id){
		$data = array();
		$data['menu'] = "Quản Lí Task Nhỏ Đồ Án";
		$data['title'] = 'Danh Sách Task';
		$data['data'] = 'leader/manage_small_task/LoadData';
		$data['status_menu'] = array("exp","exp","active exp");
		$data['link_add'] = 'http://localhost/CodeIgniter_Manage_Project/leader/ManageSmallTask/loadViewAddSmallTask?big_task_id='.$big_task_id."&project_id=".$project_id;
		return $data;
	}

	public function getGeneralDataSmallTask($big_task_id){
		$query = $this->db->query("SELECT * FROM small_task WHERE big_task_id=".$big_task_id);
		$result = $query->result_array();
		return $result;
	}

	public function deleteSmallTask($small_task_id){			
		$this->db->query("DELETE FROM small_task WHERE small_task_id='".$small_task_id."'");
	}

	public function updateSmallTask($small_task_id,$small_task){
		$this->db->where("small_task_id",$small_task_id);
		$this->db->update("small_task",$small_task);
	}

	public function addSmallTask($small_task){
		$this->db->insert("small_task",$small_task);
	}
}