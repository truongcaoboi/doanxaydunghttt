<?php
class BigTask extends CI_Model{
	function __construct(){
		$this->load->database();
	}

	public function variableStatic($project_id){
		$data = array();
		$data['menu'] = "Quản Lí Task Đồ Án";
		$data['title'] = 'Danh Sách Big Task';
		$data['data'] = 'admin/manage_big_task/LoadData';
		$data['status_menu'] = array("exp","exp","active exp","exp","exp");
		$data['link_add'] = 'http://localhost/CodeIgniter_Manage_Project/admin/ManageBigTask/loadViewAddBigTask?on_project='.$project_id;
		return $data;
	}

	public function getGeneralDataBigTask($on_project){
		$query = $this->db->query("SELECT * FROM big_task WHERE on_project=".$on_project);
		$result = $query->result_array();
		return $result;
	}

	public function deleteBigTask($big_task_id){			
		$this->db->query("DELETE FROM big_task WHERE big_task_id='".$big_task_id."'");
	}

	public function updateBigTask($big_task_id,$big_task){
		$this->db->where("big_task_id",$big_task_id);
		$this->db->update("big_task",$big_task);
	}

	public function addBigTask($big_task){
		$this->db->insert("big_task",$big_task);
	}
}