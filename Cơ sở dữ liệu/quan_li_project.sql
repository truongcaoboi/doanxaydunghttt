-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Máy chủ: 127.0.0.1
-- Thời gian đã tạo: Th9 29, 2019 lúc 10:27 AM
-- Phiên bản máy phục vụ: 10.1.37-MariaDB
-- Phiên bản PHP: 7.1.24

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Cơ sở dữ liệu: `quan_li_project`
--

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `big_task`
--

CREATE TABLE `big_task` (
  `big_task_id` int(11) NOT NULL,
  `big_task_des` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `big_task_deadline` date DEFAULT NULL,
  `project_id` int(11) NOT NULL,
  `big_task_name` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_date` datetime NOT NULL,
  `created_by` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `big_task`
--

INSERT INTO `big_task` (`big_task_id`, `big_task_des`, `big_task_deadline`, `project_id`, `big_task_name`, `created_date`, `created_by`) VALUES
(1, 'Thuc hien de xuat de tai va lap ke hoach', '2019-09-12', 0, '', '0000-00-00 00:00:00', 0);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `comment`
--

CREATE TABLE `comment` (
  `comment_id` int(11) NOT NULL,
  `comment_content` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `big_task_id` int(11) NOT NULL,
  `small_task_id` int(11) NOT NULL,
  `created_date` datetime NOT NULL,
  `created_by` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `document`
--

CREATE TABLE `document` (
  `document_id` int(11) NOT NULL,
  `document_name` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `document_path` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `big_task_id` int(11) DEFAULT NULL,
  `small_task_id` int(11) DEFAULT NULL,
  `comment_id` int(11) DEFAULT NULL,
  `created_date` datetime NOT NULL,
  `created_by` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `human`
--

CREATE TABLE `human` (
  `human_id` int(11) NOT NULL,
  `human_code` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `human_name` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `human_image` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `human_email` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `human_facebook` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `human_phone` varchar(12) COLLATE utf8mb4_unicode_ci NOT NULL,
  `human_type` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `project`
--

CREATE TABLE `project` (
  `project_name` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `topic_name` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `topic_des` varchar(2000) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `project_id` int(11) NOT NULL,
  `project_code` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `teacher_id` int(11) NOT NULL,
  `subject_id` int(11) NOT NULL,
  `leader_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `project`
--

INSERT INTO `project` (`project_name`, `topic_name`, `topic_des`, `project_id`, `project_code`, `teacher_id`, `subject_id`, `leader_id`) VALUES
('nhom 12', 'Quan ly do an sinh vien', 'Lap trinh web quan ly sinh vien', 1, '', 0, 0, 0);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `project_student`
--

CREATE TABLE `project_student` (
  `project_id` int(11) NOT NULL,
  `student_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `project_student`
--

INSERT INTO `project_student` (`project_id`, `student_id`) VALUES
(0, 20154018),
(0, 20154219);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `small_task`
--

CREATE TABLE `small_task` (
  `smal_task_id` int(11) NOT NULL,
  `big_task_id` int(11) NOT NULL,
  `small_task_name` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `small_task_deadline` datetime NOT NULL,
  `small_task_des` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `assign_to` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `student`
--

CREATE TABLE `student` (
  `student_name` varchar(2000) COLLATE utf8mb4_unicode_ci NOT NULL,
  `student_code` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `student_class` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `student_image` varchar(2000) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `facebook` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `student_course` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `id` int(11) NOT NULL,
  `student_id` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `student`
--

INSERT INTO `student` (`student_name`, `student_code`, `student_class`, `student_image`, `email`, `facebook`, `student_course`, `phone`, `id`, `student_id`) VALUES
('Dxtruong', '', 'cntt2.1', '', 'hotro@dxt.com.vn', 'http://facebook.com/~abc', 'K60', '0123456789', 1, '20154018'),
('VMTu', '', 'cntt2.1', '', 'vutuminh@gmail.com', 'http://facebook.com/~abc', 'K60', '0123456789', 2, '20154219');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `subject`
--

CREATE TABLE `subject` (
  `subject_name` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL,
  `subject_id` int(11) NOT NULL,
  `subject_code` varchar(6) COLLATE utf8mb4_unicode_ci NOT NULL,
  `subject_des` text COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `subject`
--

INSERT INTO `subject` (`subject_name`, `subject_id`, `subject_code`, `subject_des`) VALUES
('project 1', 1, '', '');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `user`
--

CREATE TABLE `user` (
  `user_id` int(11) NOT NULL,
  `user_type` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_name` varchar(2000) COLLATE utf8mb4_unicode_ci NOT NULL,
  `pass_word` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `user`
--

INSERT INTO `user` (`user_id`, `user_type`, `user_name`, `pass_word`, `is_active`) VALUES
(1, '0', 'admin', '123', 0),
(4, '2', '20154018', '20154018', 0),
(5, '1', '20154219', '20154219', 0);

--
-- Chỉ mục cho các bảng đã đổ
--

--
-- Chỉ mục cho bảng `big_task`
--
ALTER TABLE `big_task`
  ADD PRIMARY KEY (`big_task_id`);

--
-- Chỉ mục cho bảng `comment`
--
ALTER TABLE `comment`
  ADD PRIMARY KEY (`comment_id`);

--
-- Chỉ mục cho bảng `document`
--
ALTER TABLE `document`
  ADD PRIMARY KEY (`document_id`);

--
-- Chỉ mục cho bảng `human`
--
ALTER TABLE `human`
  ADD PRIMARY KEY (`human_id`);

--
-- Chỉ mục cho bảng `project`
--
ALTER TABLE `project`
  ADD PRIMARY KEY (`project_id`);

--
-- Chỉ mục cho bảng `project_student`
--
ALTER TABLE `project_student`
  ADD PRIMARY KEY (`project_id`,`student_id`);

--
-- Chỉ mục cho bảng `small_task`
--
ALTER TABLE `small_task`
  ADD PRIMARY KEY (`smal_task_id`);

--
-- Chỉ mục cho bảng `student`
--
ALTER TABLE `student`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `subject`
--
ALTER TABLE `subject`
  ADD PRIMARY KEY (`subject_id`);

--
-- Chỉ mục cho bảng `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`user_id`);

--
-- AUTO_INCREMENT cho các bảng đã đổ
--

--
-- AUTO_INCREMENT cho bảng `big_task`
--
ALTER TABLE `big_task`
  MODIFY `big_task_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT cho bảng `comment`
--
ALTER TABLE `comment`
  MODIFY `comment_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT cho bảng `document`
--
ALTER TABLE `document`
  MODIFY `document_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT cho bảng `human`
--
ALTER TABLE `human`
  MODIFY `human_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT cho bảng `project`
--
ALTER TABLE `project`
  MODIFY `project_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT cho bảng `small_task`
--
ALTER TABLE `small_task`
  MODIFY `smal_task_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT cho bảng `student`
--
ALTER TABLE `student`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT cho bảng `subject`
--
ALTER TABLE `subject`
  MODIFY `subject_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT cho bảng `user`
--
ALTER TABLE `user`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
